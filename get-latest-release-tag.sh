#!/usr/bin/env sh
GITHUB_REPOSITORY_URL="$1"

LATEST_RELEASE_URL="${GITHUB_REPOSITORY_URL}/releases/latest"
LATEST_RELEASE_TAG="$(curl -sI "${LATEST_RELEASE_URL}" | grep -i '^Location:' | tr -d '[:space:]' | awk -F'/tag/' '{print $2}')"

echo -n "${LATEST_RELEASE_TAG}"
